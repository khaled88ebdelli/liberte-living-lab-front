import mapboxgl from 'mapbox-gl/dist/mapbox-gl.js';

mapboxgl.accessToken = 'pk.eyJ1IjoiZWJkZWxsaSIsImEiOiJjazg3MG9xcGowODM2M21zOTl3Z2d0M3dhIn0.fE0b59LqQjVnd4dev89Mhg';
export const mapBox = (lat, lng) => {
    const map = new mapboxgl.Map({
        container: 'modal-content',
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [lng, lat],
        zoom: 13,
    })
   new mapboxgl.Marker()
  .setLngLat([lng, lat])
  .addTo(map);

  return map
}