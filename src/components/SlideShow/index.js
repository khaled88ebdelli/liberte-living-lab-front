import React from 'react';
import { Zoom } from 'react-slideshow-image';
import './style.css';


const zoomOutProperties = {
    duration: 5000,
    transitionDuration: 500,
    infinite: true,
    indicators: true,
    arrows: true,
    scale: 0.8,
    pauseOnHover: true,
    onChange: (oldIndex, newIndex) => {
        console.log(`slide transition from ${oldIndex} to ${newIndex}`);
    }
}

const Slideshow = ({ slideImages }) => {
    return (
        <div className="slide-container">
          { Array.isArray(slideImages) && slideImages.length > 0 && <Zoom {...zoomOutProperties}>
          { 
            slideImages.map((each, index) => <img className="img" key={index} style={{width: "100%"}} src={each} alt="" />)
          }
        </Zoom>}
        </div>
    )
}
export default Slideshow;