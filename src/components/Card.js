import React from 'react'

function Card({title, image, price }) {
    return (
        <div className="card">
            <div className="bg-img">
                <img className="img" alt="card-img" src={image}/>
                <div className="content">
                    <h4>{title}</h4>
                    <h5>{price} euro</h5>
                </div>
            </div>
        </div>
    )
}


export default Card

