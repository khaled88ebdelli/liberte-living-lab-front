import React from 'react'
import '../../components/Modal/style.scss'

function Modal({ title, children, close, show }) {
    if (!show) {
        return null;
    }
    return (
        <div className="modal" id="modal">
            <h2>{title}</h2>
            <div className="content">
            <div id="modal-content" ></div>
                {children}
            </div>
            <div className="actions">
                <button className="toggle-button" onClick={close}>Close</button>
            </div>
        </div>
    )
}

export default Modal
