import React, { useState } from 'react';
import './Search.module.css';
import { useSelector, useDispatch } from 'react-redux';
import { SearchThunk, selectSearchData } from './searchSlice';
import Card from '../../components/Card';
import Modal from '../../components/Modal';
import { mapBox } from '../../components/mapBox';
import Slideshow from '../../components/SlideShow';


const noImage = 'https://www.collegecitoyen.ca/wp-content/uploads/2019/05/no-image.png'

export const Search = () => {
  const { data, loading, error, ...rest } = useSelector(selectSearchData);
  const dispatch = useDispatch();
  const [department, setDepartment] = useState(rest?.departement);
  const [minprice, setMinprice] = useState(rest?.minprice);
  const [maxprice, setMaxprice] = useState(rest?.maxprice);
  const [show, setShow] = useState(false);
  const [selected, setSelected] = useState({});
  const [clickMarker, setClickMarker] = useState(false);

  const departementArray = [
    {
      label: 'Alpes-de-Haute-Provence',
      key: 'alpes_de_haute_provence'
    },
    {
      label: 'Ille-et-Vilaine',
      key: 'ille_et_vilaine'
    },
    {

      label: 'Pyrénées-Orientales',
      key: 'pyrenees_orientales'
    }
  ]
  const handleSubmit = (e) => {
    e.preventDefault()
    dispatch(SearchThunk({ department, minprice, maxprice }))
  }
  const showModal = (selected) => {
    console.log('selected', selected);
    setShow(true)
    setSelected(selected)
    setTimeout(() => {
      const map = mapBox(selected?.location?.lat, selected?.location?.lng);
      map.on('click', function (e) {
        console.log('marker', e);
        setClickMarker(true)
      })
    }, 1000)
  }
  const closeModal = () => {
    setShow(false)
    setSelected({})
    setClickMarker(false)
  }
  return (
    <>
      <div align="center" className="form-container">
        <form className="form" onSubmit={handleSubmit} >
          <div className="form-group row">
            <label className="col-3" htmlFor="departement"> </label>
            <div className="col-9">
              <input className="form-control" type="search" id="search" placeholder="Search..." />
            </div>
          </div>
          <div className="form-group row">
            <label className="col-3" htmlFor="departement">Département </label>
            <div className="col-9">
              <select className="form-control" name="departement" id="departement"
                onChange={e => setDepartment(e.target.value)}>
                {departementArray.map(item => <option key={item.key} value={item.key}>{item.label}</option>)}
              </select>
            </div>
          </div>
          <div className="form-group row">
            <label className="col-3" htmlFor="minprice">Prix min </label>
            <div className="col-9">
              <input className="form-control" type="number" name="minprice" value={minprice} id="minprice"
                onChange={e => setMinprice(e.target.value)} />
            </div>
          </div>
          <div className="form-group row">
            <label className="col-3" htmlFor="maxprice">Prix max </label>
            <div className="col-9">
              <input className="form-control" type="number" name="maxprice" value={maxprice} id="maxprice"
                onChange={e => setMaxprice(e.target.value)} />
            </div>
          </div>
          <div className="form-group row">
            <input type="submit" className="btn" value="Recherche" />
          </div>



        </form>
        <Modal
          title={selected.title}
          close={closeModal}
          show={show}>
          {clickMarker && <Slideshow slideImages={selected.images} />}
        </Modal>
      </div>
      <div className="grid-container col-container ">
        {data.length > 0 && data.map((item, index) => {
          return (
            <div className="col" key={index} onClick={() => showModal(item)}>
              <Card
                title={item?.location?.region_name}
                image={item.images ? item.images[0] : noImage}
                price={item.price} />
            </div>)
        })}
      </div>
      {loading && <h5>loading...</h5>}
    </>
  );
}