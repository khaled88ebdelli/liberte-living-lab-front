import { createSlice } from '@reduxjs/toolkit';
import axios from 'axios';

export const slice = createSlice({
  name: 'search',
  initialState: {
    loading: false,
    data: [],
    nbResult: '',
    department: '',
    minprice: '',
    maxprice: '',
    error: null
  },
  reducers: {
    fetchStart: (state, action) => {
      console.log(action);
      state.department = action.payload.department;
      state.minprice = action.payload.minprice;
      state.maxprice = action.payload.maxprice;
      state.loading = true;
    },
    fetchSuccess: (state, action) => {
      state.data = action.payload.results;
      state.nbResult= action.payload.nbResult;
      state.loading = false;
    },
    fetchFailed: (state, action) => {
      console.log(action);
      state.error = action.payload.err;
      state.loading = false;
    },
  },
});

export const { fetchStart, fetchSuccess, fetchFailed } = slice.actions;

export const SearchThunk = ({department, minprice, maxprice}) => dispatch => {
  dispatch(fetchStart({department, minprice, maxprice}));
  axios.get(`/api?minprice=${minprice}&maxprice=${maxprice}&department=${department}`)
  .then(({data}) => {
    console.log(data);
    dispatch(fetchSuccess(data));
  })
  .catch(err => {
    console.log(err);
    dispatch(fetchFailed(err.message))
  })
};
// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state) => state.counter.value)`
export const selectSearchData = state => state.search;

export default slice.reducer;
